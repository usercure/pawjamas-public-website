<?php
/*
 * Template Name: Custom Single Template
 * Template Post Type: post
 */

 get_header();  ?>



 	<!-- <div id="primary" <?php //astra_primary_class(); ?>>

 		<?php //astra_primary_content_top(); ?>

 		<?php //astra_content_loop(); ?>

 		<?php //astra_primary_content_bottom(); ?>

 	</div> #primary -->


  <section class="single-blog-content">

      <div class="wrap-content blog-single">

      <?php  while ( have_posts() ) : the_post(); ?>
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="post-thumbnail"><?php the_post_thumbnail(array(250, 250)); ?> </div>
              <?php the_title( '<h1>','</h1>' );  ?>
              <div class="entry-content"><?php the_content(); ?></div>
          </article>
      <?php endwhile; ?>

      </div>

  </section>


 <?php get_footer(); ?>
